In the Hall of the Mountain King

Music
-----
Arrangement: Trent Reznor & Atticus Ross
Album: The Social Network (2010)
Original Melody by Edvard Grieg (1876)


Banner Art
-----
Image: http://www.deviantart.com/art/The-Hall-of-the-Mountain-King-202101550
Image Artist: ImmortalKhan
Font: Code (http://www.dafont.com/code.font)
Font Artist: Fontfabric (http://www.fontfabric.com/)


Puzzles
-----
The puzzles were inspired by The Witness, a 2016 video game by Thekla, Inc.,
If you haven't played it I encourage you to do so!  As of this writing, it
has been released for PC, PS4, and Xbox One.

The Lua used to evaluate players' solutions is entirely my own.

The puzzles that appear in this simfile are hardcoded (you can inspect the
"puzzles" folder) but their solutions are not.  Many puzzles have multiple
valid solutions.

While creating and testing the puzzles that appear in this simfile,
I made use of https://windmill.thefifthmatt.com/build, a fan-made
site for building and sharing puzzles inspired by The Witness.


Controls
-----
Use the pad's four arrows to draw a line that satisfies the rules of
the puzzles.  You'll start drawing from the large white circle and make
your way to the "nub" sticking out of the grid.

You can use SELECT (the red button on ITG dedicabs) to reset your
current puzzle back to its initial state if you need to.

Good luck and have fun!

Dan / dbk2
November 28, 2016