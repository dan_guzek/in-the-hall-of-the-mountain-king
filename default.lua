-- ------------------------------------------------------
-- Stuff related to random chart selection

local reload_screen = false
local steps = GAMESTATE:GetCurrentSong():GetStepsByStepsType( "StepsType_Dance_Single" )

for player in ivalues( GAMESTATE:GetHumanPlayers() ) do
	local difficulty = GAMESTATE:GetCurrentSteps(player):GetDifficulty()
	if difficulty == 'Difficulty_Challenge' then
		local r = math.random(2,#steps)
		GAMESTATE:SetCurrentSteps(player, steps[r])
		table.remove(steps, r)
		reload_screen = true
	end
end

if reload_screen then
	return Def.ActorFrame {
		OnCommand=function(self)
			if SCREENMAN:GetTopScreen():GetName() ~= "ScreenEdit" then
				SCREENMAN:SetNewScreen('ScreenGameplay')
			end
		end
	}
end

-- ------------------------------------------------------
-- Variables

local _line_thickness = 10
local _inner_grid_width = _line_thickness * 4

local song_dir = GAMESTATE:GetCurrentSong():GetSongDir()

local puzzle_types = {"rects", "suns", "mixed"}
local puzzle_filenames = {}

for _,v in ipairs(puzzle_types) do
	puzzle_filenames[v] = {}

	-- FILEMAN is less-than-helpful here since it returns OS-specific hidden files
	local _files = FILEMAN:GetDirListing(song_dir.."puzzles/"..v.."/")

	-- so loop through all of what it returns and only add it to the
	-- puzzles table if it matches the pattern of "%d+.lua"
	for i,filename in ipairs(_files) do
		if string.match(filename, "%d+.lua") then
			table.insert(puzzle_filenames[v], filename)
		end
	end
end

-- ------------------------------------------------------
-- Input Handler (only used to listen for SELECT presses)

local InputHandler = function(event)
	if not event.PlayerNumber or not event.button then
		return false
	end

	if event.type == "InputEventType_FirstPress" then
		if event.GameButton == "Select" then
			local pn = ToEnumShortString(event.PlayerNumber)
			local af = SCREENMAN:GetTopScreen():GetChild("SongForeground"):GetChild("default.lua")
			af:playcommand(pn.."Cancel")
			af:queuecommand(pn.."ResetVertices")
		end
	end
end

-- ------------------------------------------------------
-- Actors

-- main FGChange ActorFrame
local af = Def.ActorFrame{
	Name="MainAF",
	OnCommand=function(self)
		SCREENMAN:GetTopScreen():AddInputCallback( InputHandler )
	end
}

-- keep-alive Actor
af[#af+1] = Def.Actor{ InitCommand=function(self) self:sleep(999) end }

-- background Sprite
af[#af+1] = Def.Sprite{
	Texture="./img/_bg.png",
	InitCommand=function(self)
		self:FullScreen():Center()
	end,
	UnhideP1Command=function(self)
		self:linear(0.125):cropleft(0.5)
	end,
	UnhideP2Command=function(self)
		self:linear(0.125):cropright(0.5)
	end
}

-- each player...
for player in ivalues(GAMESTATE:GetHumanPlayers()) do

	-- ...gets one of each type of puzzle
	for i, puzzle_type in ipairs(puzzle_types) do

		-- get a random int no larger than the number of puzzles for this type
		local r = math.random( #puzzle_filenames[puzzle_type] )
		-- use that random int to get the corresponding filename
		local filename = puzzle_filenames[puzzle_type][r]

		-- remove this puzzle from the list so that
		-- the next player can't be assigned it as well
		table.remove(puzzle_filenames[puzzle_type], r)

		local _puzzle_info = LoadActor("./puzzles/"..puzzle_type.."/"..filename)

		local args = {
			player=player,
			line_thickness=_line_thickness,
			inner_grid_width=_inner_grid_width,
			puzzle_info=_puzzle_info,
			puzzle_num=i,
			total_num_puzzles=#puzzle_types,
		}

		af[#af+1] = LoadActor("./puzzle.lua", args)..{
			Name=ToEnumShortString(player).."_Puzzle_AF_"..i,
			InitCommand=function(self)
				-- hide later puzzles for now
				if i ~= 1 then self:visible(false)
				else self:queuecommand("ActivatePuzzle")
				end

				local offset = _puzzle_info.grid.w == 5 and -128 or -104
				local direction = player == PLAYER_1 and -1 or 1

				self:x( offset + _screen.cx + (direction * (_screen.w*160/640)) )
					:y( _screen.cy-150 )
					:halign(0.5)
			end
		}
	end
end

return af