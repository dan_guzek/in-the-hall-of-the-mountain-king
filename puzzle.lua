local args = ...
local player = args.player
local line_thickness = args.line_thickness
local inner_grid_width = args.inner_grid_width
local grid_unit = line_thickness + inner_grid_width
local puzzle_number = args.puzzle_num
local total_num_puzzles = args.total_num_puzzles

local GRID = args.puzzle_info.grid
local START_POINT = args.puzzle_info.start_point
local END_POINT = args.puzzle_info.end_point
local PIECES = args.puzzle_info.pieces

-- Is this particular puzzle actively being worked on?
-- Used to prevent hidden puzzles from being unintentionally worked on by StepMessageCommand.
local ACTIVE = false

-- The player's solution is "innocent until proven guilty."
local SOLUTION_IS_CORRECT = true

-- Data structures used throughout.
-- If the player's solution is incorrect, these all need to be reset in the Initialize() function below.
local segments, verts, matrix, vertical_segments, horizontal_segments, accounted_for, chunks, current_chunk, incorrect_spots

-- Flags used in line management logic, mostly to bail out early if possible.
local can_only_move, at_endpoint

-- incorrect_quads is a table that contains references to the Quads behind each
-- grid piece; it is used in VerifySolution() to flash incorrect grid pieces.
local incorrect_quads = {}
for i=1, GRID.h do
	incorrect_quads[i] = {}
end


local Initialize = function()
	SOLUTION_IS_CORRECT = true

	-- a stack of coordinates representing where the line is and has been
	-- datapoints will be like {{1,1}, {1,2}, {2,2}}
	-- this is mostly used to check bounds when drawing the line
	segments = { START_POINT }

	-- a stack that contains the pixel data of where to draw the AMV line
	verts= {
		{{0, 0, 0}, Color.White},
	}

	-- a true/false matrix used to determine if the point we want to move to
	-- already has a point there from a previous line segment.
	matrix = {}
	for i=1, GRID.h+1 do
		matrix[i] = {}

		for j=1, GRID.w+1 do
			matrix[i][j] = false
		end
	end
	-- seed the matrix with the START_POINT set to true
	matrix[START_POINT[1]+1][START_POINT[2]+1] = true


	-- additional two true/false matrices used in the chunking routine
	vertical_segments, horizontal_segments = {}, {}

	for i=1, GRID.h do
		vertical_segments[i] = {}
		for j=1, GRID.w+1 do
			vertical_segments[i][j] = false
		end
	end
	for i=1, GRID.h+1 do
		horizontal_segments[i] = {}
		for j=1, GRID.w do
			horizontal_segments[i][j] = false
		end
	end

	-- can_only_move will be one of {nil,1,2,3,4} and represents the only direction a line can go next
	can_only_move = nil

	-- at_endpoint will be false or true and helps us cancel additional
	-- moves when a puzzle's solution is being verified
	at_endpoint = false

	accounted_for, chunks, current_chunk, incorrect_spots = {}, {}, {}, {}

	for i=1, GRID.h+1 do
		accounted_for[i] = {}
		for j=1, GRID.w+1 do
			accounted_for[i][j] = false
		end
	end

	for i=1, GRID.h do
		incorrect_spots[i] = {}
		for j=1, GRID.w do
			incorrect_spots[i][j] = false
		end
	end
end

Initialize()



-- for debugging
local print_matrix = function( _matrix )
	local s = ""
	for i,row in pairs(_matrix) do
		for j,val in pairs(row) do
			if val then val = "true "
			else val = "false" end
			s = s .. val .. ", "
		end
		s = s .. "\n"
	end
	SCREENMAN:SystemMessage( s )
end

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- variables and functions used in the line-management routine

local OppositeDirection = {4,3,2,1}

local CurrentPoint = function()
	return {segments[#segments][1], segments[#segments][2]}
end

local IsNextToEndPoint = function()
	return (CurrentPoint()[1]==END_POINT[1] and CurrentPoint()[2]==END_POINT[2])
end

-- In what direction off the puzzle is the EndPoint facing? One of {1,2,3,4}
local EndPointIsFacing = nil

local WillBeWithinBounds = function(_dir)
	if _dir == 1 then return segments[#segments][2] > 0 end
	if _dir == 2 then return segments[#segments][1] < GRID.h end
	if _dir == 3 then return segments[#segments][1] > 0 end
	if _dir == 4 then return segments[#segments][2] < GRID.w end
end

local IsBackingOut = function(_dir)
	if _dir == 1 then return ((#segments-1 > 0) and (segments[#segments][2]-1 == segments[#segments-1][2])) end
	if _dir == 2 then return ((#segments-1 > 0) and (segments[#segments][1]+1 == segments[#segments-1][1])) end
	if _dir == 3 then return ((#segments-1 > 0) and (segments[#segments][1]-1 == segments[#segments-1][1])) end
	if _dir == 4 then return ((#segments-1 > 0) and (segments[#segments][2]+1 == segments[#segments-1][2])) end
end

local IsNextSpotOccupied = function(_dir)
	if _dir == 1 then return matrix[segments[#segments][1]+1][segments[#segments][2]] end
	if _dir == 2 then return matrix[segments[#segments][1]+2][segments[#segments][2]+1] end
	if _dir == 3 then return matrix[segments[#segments][1]][segments[#segments][2]+1] end
	if _dir == 4 then return matrix[segments[#segments][1]+1][segments[#segments][2]+2] end
end

local AddToVerts = function(_dir, amount)
	if _dir == 1 then table.insert( verts, {{verts[#verts][1][1] - amount, verts[#verts][1][2], 0}, Color.White} ) end
	if _dir == 2 then table.insert( verts, {{verts[#verts][1][1], verts[#verts][1][2] + amount, 0}, Color.White} ) end
	if _dir == 3 then table.insert( verts, {{verts[#verts][1][1], verts[#verts][1][2] - amount, 0}, Color.White} ) end
	if _dir == 4 then table.insert( verts, {{verts[#verts][1][1] + amount, verts[#verts][1][2], 0}, Color.White} ) end
end

local AddToSegments = function(_dir)
	if _dir == 1 then table.insert( segments, {segments[#segments][1], segments[#segments][2] - 1} ) end
	if _dir == 2 then table.insert( segments, {segments[#segments][1] + 1, segments[#segments][2]} ) end
	if _dir == 3 then table.insert( segments, {segments[#segments][1] - 1, segments[#segments][2]} ) end
	if _dir == 4 then table.insert( segments, {segments[#segments][1], segments[#segments][2] + 1} ) end
end

local ManageSegmentMatrices = function(_dir, _val)
	if _dir == 1 then horizontal_segments[segments[#segments][1]+1][segments[#segments][2]+1] = _val end
	if _dir == 2 then vertical_segments[segments[#segments][1]][segments[#segments][2]+1] = _val end
	if _dir == 3 then vertical_segments[segments[#segments][1]+1][segments[#segments][2]+1] = _val end
	if _dir == 4 then horizontal_segments[segments[#segments][1]+1][segments[#segments][2]] = _val end
end
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


local CheckPiece
CheckPiece = function( d,r )
	-- first ensure that this check will be within bounds
	if (r <= GRID.w) and (d <= GRID.h) and (d > 0) and (r > 0) then

		-- then ensure that we haven't already inspected this grid piece
		if not accounted_for[d][r] then
			accounted_for[d][r] = true

			-- if no line segment exists to the right
			if not vertical_segments[d][r+1] then
				CheckPiece( d, r+1 )
			end

			-- if no line segment exists to the bottom
			if not horizontal_segments[d+1][r] then
				CheckPiece( d+1, r )
			end

			-- if no line segment exists to the top
			if not horizontal_segments[d][r] then
				CheckPiece( d-1, r )
			end

			-- if no line segment exists to the left
			if not vertical_segments[d][r] then
				CheckPiece( d, r-1 )
			end


			local kind = PIECES[d][r][1]
			local color = PIECES[d][r][2]

			if kind and color then
				if current_chunk[kind] == nil then current_chunk[kind] = {} end
				if current_chunk[kind][color] == nil then current_chunk[kind][color] = {} end
				if current_chunk.by_color == nil then current_chunk.by_color = {} end
				if current_chunk.by_color[color] == nil then current_chunk.by_color[color] = {} end

				table.insert( current_chunk[kind][color], {d=d,r=r} )
				table.insert( current_chunk.by_color[color], {d=d,r=r,kind=kind} )
			end
		end
	end
end

local ValidateCurrentChunk = function()

	-- Validate rects if this chunk has them.
	if current_chunk.rect then

		-- First, assess whether this chunk has rects of more than a single color.
		local more_than_one_color = false
		for k,v in pairs(current_chunk.rect) do
			if more_than_one_color then
				SOLUTION_IS_CORRECT = false
				break
			end

			more_than_one_color = true
		end

		-- this chunk has rects of more than a single color, so it's incorrect
		-- and we need to flash colored rects with lesser quantities so the player
		-- undertands that those are incorrect
		if more_than_one_color then
			--  First, determine which color rect this chunk has the most of.
			local most_rects
			for k,v in pairs(current_chunk.rect) do
				if most_rects == nil then most_rects = k end
				if (#v > #current_chunk.rect[most_rects]) then most_rects = k end
			end

			-- then loop through all other colors of rects in this chunk and mark them as incorrect
			for k,v in pairs(current_chunk.rect) do
				if k ~= most_rects then
					for i=1,#v do
						incorrect_spots[ v[i].d ][ v[i].r ] = true
					end
				end
			end
		end
	end

	-- Validate suns if this chunk has them.
	if current_chunk.sun then
		for k,v in pairs(current_chunk.sun) do
			if #current_chunk.by_color[k] ~= 2 then

				SOLUTION_IS_CORRECT = false

				for i=1,#current_chunk.by_color[k] do
					local _piece = current_chunk.by_color[k][i]
					incorrect_spots[ _piece.d ][ _piece.r ] = true
				end
			end
		end
	end

end


local FindChunks = function()
	for d=1, GRID.h do
		for r=1, GRID.w do
			if not accounted_for[d][r] then
				CheckPiece( d,r )

				table.insert( chunks, current_chunk )

				-- ValidateCurrentChunk() will both assess the solution for correctness
				-- and determine which grid spots to flash red to indicate incorrectness, if necessary.
				ValidateCurrentChunk()

				current_chunk = {}
			end
		end
	end
end



local VerifySolution = function()
	-- The player has effectively submitted an answer; time to verify it.
	-- Start by breaking the grid into chunks based on what line the player drew
	-- and verify the solution based on whether each of those chunks abides by the rules.
	FindChunks()

	if SOLUTION_IS_CORRECT then
		-- the solution was correct...
		local MainAF = SCREENMAN:GetTopScreen():GetChild("SongForeground"):GetChild("default.lua")
		-- ...so hide the current puzzle
		MainAF:GetChild( ToEnumShortString(player).."_Puzzle_AF_"..puzzle_number):playcommand("PuzzleComplete")

		if puzzle_number < total_num_puzzles then
			-- and reveal the next
			MainAF:GetChild( ToEnumShortString(player).."_Puzzle_AF_"..(puzzle_number+1) ):queuecommand("ActivatePuzzle")

		elseif puzzle_number == total_num_puzzles then
			-- and it was the final puzzle, so reveal this player's playfield.
			MainAF:queuecommand("Unhide"..ToEnumShortString(player))
		end

	else
		-- The player's solution was incorrect
		-- so flash the spots marked as incorrect
		for i=1, #incorrect_spots do
			for j=1, #incorrect_spots[i] do
				if incorrect_spots[i][j] then
					incorrect_quads[i][j]:queuecommand("Flash")
				end
			end
		end
		-- and re-initialize this current puzzle.
		Initialize()
	end
end

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

-- there are three things to check for when managing the line:
-- 1. if extending the line in any direction would cause it to go out of bounds, don't do anything
-- 2. if we're going backwards (undoing the previous move), pop it off the stack(s) and update the AMV
-- 3. don't allow lines to overlap

local ExtendLine = function(_dir)
	-- If the player has already reached the endpoint from a previous move, don't do anything.
	if at_endpoint then return end

	-- Don't allow lines segments to overlap!
	-- If can_only_move is set from the previous move
	-- and the player is trying to move some other direction
	-- then just exit the function now; don't do anything.
	if (can_only_move and can_only_move ~= _dir) then return end

	-- If the player is next to an endpoint and wants to move toward it
	if IsNextToEndPoint() and _dir==EndPointIsFacing then
		at_endpoint = true
		AddToVerts(_dir, line_thickness)

		VerifySolution()
		return
	end

	-- Normally, don't allow the line to go outside of the puzzle grid.
	if WillBeWithinBounds(_dir) then
		-- If we're backing out to a spot on the grid we were just at...
		if IsBackingOut(_dir) then
			-- Typically, we want to mark the previous spot as "unoccupied" (that is, false).
			-- We don't want to this if we're about to back out of a half-move.
			if can_only_move == nil then
				-- Flag the spot we just backed out of as unoccupied.
				matrix[segments[#segments][1]+1][segments[#segments][2]+1] = false
			end
			-- We are backing out of a spot.  The line is losing its most recent segment.
			-- Pop the last item off the verts and segments stacks.
			table.remove(verts)
			table.remove(segments)

			if can_only_move == nil then
				ManageSegmentMatrices(_dir, false)
			end
		else
			-- If moving forward would cause us to land on an already occupied spot
			if IsNextSpotOccupied(_dir) then

				-- then do a "half-move"
				AddToVerts(_dir, inner_grid_width/2)
				AddToSegments(_dir)

				-- Set a flag that we've just performed a half move that won't
				-- reset to nil until we undo the half-move.
				can_only_move = OppositeDirection[_dir]
				return
			else
				-- We're performing a "normal" move forward.  We are pushing one new segment to our stack.
				AddToVerts(_dir, grid_unit)
				AddToSegments(_dir)
				ManageSegmentMatrices(_dir, true)

				-- Mark the new point we've landed on as occupied.
				matrix[segments[#segments][1]+1][segments[#segments][2]+1] = true
			end
		end
		can_only_move = nil
	end
end


-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Actors

local puzzle = Def.ActorFrame{
	[ToEnumShortString(player).."CancelCommand"]=function(self)
		if ACTIVE then
			Initialize()
		end
	end
}

-- background Quads that will flash red if this particular piece
-- is not satisfactory as part of the larger solution
for i=1, GRID.h do
	for j=1, GRID.w do
		puzzle[#puzzle+1] = Def.Quad{
			InitCommand=function(self)
				self:zoomto( inner_grid_width, inner_grid_width)
					:xy( grid_unit*j, grid_unit*i )
					:halign(1):valign(1)
					:diffuse(1,0,0,0)

				incorrect_quads[i][j] = self
			end,
			FlashCommand=function(self)
				self:diffuse(1,0,0,1):linear(2.5):diffuse(1,0,0,0)
			end
		}
	end
end

-- setup horizontal line segments
for i=0, GRID.h do
	puzzle[#puzzle+1] = Def.Quad{
		Name="HorizontalLine"..i,
		InitCommand=function(self)
			self:zoomto( (line_thickness * (GRID.w+1)) + (inner_grid_width * GRID.w), line_thickness)
				:xy( 0, grid_unit*i )
				:halign(0):valign(0)
				:diffuse(Color.Black)
		end
	}
end

-- setup vertical line segments
for i=0, GRID.w do
	puzzle[#puzzle+1] = Def.Quad{
		Name="VerticalLine"..i,
		InitCommand=function(self)
			self:zoomto( line_thickness, (line_thickness * (GRID.h+1)) + (inner_grid_width * GRID.h))
				:xy( grid_unit*i, 0 )
				:halign(0):valign(0)
				:diffuse(Color.Black)
		end
	}
end

-- start point (the circle)
puzzle[#puzzle+1] = Def.Sprite{
	Name="StartPoint",
	Texture="./img/circle.png",
	InitCommand=function(self)

		if START_POINT[1] < 0 or START_POINT[1] > GRID.h or START_POINT[2] < 0 or START_POINT[2] > GRID.w then
			SCREENMAN:SystemMessage( "START_POINT is outside of the grid." )
		end

		self:zoom( 0.333 )
			:valign(0.333)
			:halign(0.333)
			:x( grid_unit*(START_POINT[2]) )
			:y( grid_unit*(START_POINT[1]) )
	end
}

-- end point
puzzle[#puzzle+1] = Def.Quad{
	Name="EndPoint",
	InitCommand=function(self)

		self:diffuse(Color.Black)
			:zoomto(line_thickness, line_thickness)

		-- start and end are mistakenly identical; alert and bail.
		if END_POINT[1] == START_POINT[1] and END_POINT[2] == START_POINT[2] then
			self:rainbow()
			SCREENMAN:SystemMessage( "START_POINT and END_POINT are identical." )
			return
		end


		--top side
		if END_POINT[1] == 0 then
			EndPointIsFacing = 3
			self:xy( grid_unit*END_POINT[2], 0 )
				:halign(0):valign(1)
			return
		end

		-- bottom side
		if END_POINT[1] == GRID.h then
			EndPointIsFacing = 2
			self:xy( grid_unit*END_POINT[2], grid_unit*END_POINT[1] + line_thickness )
				:halign(0):valign(0)
			return
		end

		-- left side
		if END_POINT[2] == 0 then
			EndPointIsFacing = 1
			self:xy( -line_thickness, grid_unit*END_POINT[1] + line_thickness )
				:halign(0):valign(1)
			return
		end

		-- right side
		if END_POINT[2] == GRID.w then
			EndPointIsFacing = 4
			self:xy( grid_unit*END_POINT[2] + line_thickness, grid_unit*END_POINT[1] )
				:halign(0):valign(0)
			return
		end

		self:rainbow()
		SCREENMAN:SystemMessage( "END_POINT is not located on a puzzle edge." )
	end
}

-- pieces (suns/rects) inside the grid
for i, row in ipairs(PIECES) do
	for j, piece in ipairs(row) do

		-- if there is puzzle piece at this particular grid spot
		if piece[1] and piece[2] then
			-- then add a Sprite to the ActorFrame
			puzzle[#puzzle+1] = Def.Sprite{
				Texture="./img/"..piece[1]..".png",
				InitCommand=function(self)
					self:diffuse( Color[piece[2]] )
						:zoom(0.333)
						:x( grid_unit * j - (inner_grid_width/2) )
						:y( grid_unit * i - (inner_grid_width/2) )
				end
			}
		end
	end
end


-- player's line
puzzle[#puzzle+1] = Def.ActorMultiVertex{
	Name= "AMV_LineStrip",
	InitCommand=function(self)

		self:x( grid_unit*(START_POINT[2]) + line_thickness/2)
			:y( grid_unit*(START_POINT[1]) + line_thickness/2)
			:SetDrawState{Mode="DrawMode_LineStrip"}
			:SetLineWidth(line_thickness)
	end,
	ActivatePuzzleCommand=function(self)
		self:GetParent():visible(true)
		ACTIVE = true
	end,
	PuzzleCompleteCommand=function(self)
		self:GetParent():visible(false)
		ACTIVE = false
	end,
	StepMessageCommand=function(self, params)
		if ACTIVE and params.PlayerNumber == player then

			ExtendLine(params.Column + 1)
			-- print_matrix(incorrect_spots)

			self:SetNumVertices(#verts)
				:SetVertices(verts)
		end
	end,
	[ToEnumShortString(player).."ResetVerticesCommand"]=function(self)
		if ACTIVE then
			self:SetNumVertices(#verts):SetVertices(verts)
		end
	end
}

return puzzle