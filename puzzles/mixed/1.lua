return {
	grid = { h=3, w=3 },

	start_point = { 3,0 },

	end_point = { 3,3 },

	pieces = {
		{{"sun","Blue"},{"sun","Red"},{"rect","Yellow"}},
		{{"sun","Blue"},{"rect","Red"},{"sun","Red"}},
		{{"rect","Blue"},{"sun","Blue"},{"sun","Blue"}},
	}
}