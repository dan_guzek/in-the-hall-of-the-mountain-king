return {
	grid = { h=4, w=4 },

	start_point = { 4,2 },

	end_point = { 0,2 },

	pieces = {
		{{"rect","Black"},{"rect","Black"},{"rect","White"},{"rect","White"}},
		{{"rect","Black"},{"rect","Black"},{"rect","White"},{"rect","White"}},
		{{},{},{},{}},
		{{},{},{},{"sun","Black"}},
	}
}