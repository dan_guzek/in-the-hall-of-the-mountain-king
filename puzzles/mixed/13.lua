return {
	grid = { h=4, w=4 },

	start_point = { 2,2 },

	end_point = { 0,4 },

	pieces = {
		{{"rect","Black"},{"sun","White"},{},{"rect","White"}},
		{{},{"rect","Black"},{},{}},
		{{},{},{"rect","Black"},{"sun","White"}},
		{{"sun","Black"},{},{},{"rect","White"}},
	}
}