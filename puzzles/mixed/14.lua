return {
	grid = { h=5, w=3 },

	start_point = { 5,0 },

	end_point = { 0,3 },

	pieces = {
		{{"sun", "White"},{},{"rect","White"}},
		{{"rect","White"},{"sun", "White"},{}},
		{{"sun","Black"},{"sun", "Black"},{"sun", "Black"}},
		{{},{"sun", "White"},{}},
		{{"sun", "White"},{"rect","White"},{"rect", "Black"}},
	}
}