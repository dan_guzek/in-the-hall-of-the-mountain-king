return {
	grid = { h=4, w=4 },

	start_point = { 4,0 },

	end_point = { 0,4 },

	pieces = {
		{{"sun","Black"},{"rect", "Purple"},{"sun","Blue"},{"rect","White"}},
		{{"sun","Blue"}, {"rect","White"},{"sun","Black"},{"rect", "Purple"}},
		{{"rect","Purple"},{"sun","Black"},{"rect","White"},{"sun","Blue"}},
		{{"rect", "White"},{"sun","Blue"},{"rect","Purple"},{"sun","Black"}},
	}
}