return {
	grid = { h=5, w=5 },

	start_point = { 0,0 },

	end_point = { 5,0 },

	pieces = {
		{{"rect","White"},{},{},{},{"sun","White"}},
		{{},{"rect", "Black"},{},{"sun","White"},{}},
		{{},{},{"rect","White"},{},{}},
		{{},{"sun","White"},{},{"rect", "Black"},{}},
		{{"sun","White"},{},{},{},{"rect","White"}},
	}
}