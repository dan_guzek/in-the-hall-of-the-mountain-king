return {
	grid = { h=4, w=5 },

	start_point = { 2,0 },

	end_point = { 2,5 },

	pieces = {
		{{"sun","White"},{},{},{},{"sun","White"}},
		{{},{"rect","White"},{},{"rect","White"},{}},
		{{},{"rect","Red"},{},{"rect","Red"},{}},
		{{"sun","Red"},{},{},{},{"sun","Red"}},
	}
}