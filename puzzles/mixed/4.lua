return {
	grid = { h=4, w=5 },

	start_point = { 2,0 },

	end_point = { 2,5 },

	pieces = {
		{{"rect","Black"},{},{"sun","Black"},{"sun","White"},{}},
		{{"rect","Black"},{},{"sun","Black"},{"sun","White"},{}},
		{{},{"sun","White"},{"sun","Black"},{},{"rect","Black"}},
		{{},{"sun","White"},{"sun","Black"},{},{"rect","Black"}},
	}
}