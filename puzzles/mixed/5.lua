return {
	grid = { h=5, w=4 },

	start_point = { 0,0 },

	end_point = { 5,0 },

	pieces = {
		{{"rect","Orange"},{"rect","Blue"},{"rect","Orange"},{"rect","Blue"}},
		{{"rect","Blue"},{"sun","Orange"},{"rect","Blue"},{"sun","Orange"}},
		{{"sun","Black"},{"sun","White"},{"sun","White"},{"sun","Black"}},
		{{"sun","Orange"},{"rect","Blue"},{"sun","Orange"},{"rect","Blue"}},
		{{"rect","Blue"},{"rect","Orange"},{"rect","Blue"},{"rect","Orange"}},
	}
}