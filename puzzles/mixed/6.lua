return {
	grid = { h=4, w=4 },

	start_point = { 4,4 },

	end_point = { 3,4 },

	pieces = {
		{{"rect","Black"},{},{},{"rect","White"}},
		{{},{"sun","Black"},{"sun","White"},{}},
		{{},{"sun","White"},{"sun","Black"},{}},
		{{"rect","Black"},{},{},{"rect","White"}},
	}
}