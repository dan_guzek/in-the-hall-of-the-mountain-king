return {
	grid = { h=5, w=5 },

	start_point = { 5,0 },

	end_point = { 0,5 },

	pieces = {
		{{},{},{"rect","Black"},{},{}},
		{{},{"rect","White"},{},{"rect","White"},{}},
		{{"sun","Black"},{"rect","Black"},{"rect","White"},{"rect","Black"},{"sun","Black"}},
		{{},{"rect","White"},{},{"rect","White"},{}},
		{{},{},{"rect","Black"},{},{}},
	}
}