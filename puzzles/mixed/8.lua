return {
	grid = { h=4, w=4 },

	start_point = { 4,4 },

	end_point = { 0,0 },

	pieces = {
		{{"rect","White"},{"rect","White"},{"sun","White"},{"sun","Black"}},
		{{"rect","White"},{"rect","White"},{"sun","White"},{"sun","Black"}},
		{{"sun","White"},{"sun","Black"},{"rect","Black"},{"rect","Black"}},
		{{"sun","White"},{"sun","Black"},{"rect","Black"},{"rect","Black"}},
	}
}