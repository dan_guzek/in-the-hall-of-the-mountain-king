return {
	grid = { h=5, w=4 },

	start_point = { 0,4 },

	end_point = { 5,0 },

	pieces = {
		{{"rect","White"},{},{},{"sun","White"}},
		{{"sun","Black"},{},{"sun","White"},{"rect","White"}},
		{{"sun","Black"},{"sun","White"},{"sun","White"},{"sun","Black"}},
		{{"rect","White"},{"sun","White"},{},{"sun","Black"}},
		{{"sun","White"},{},{},{"rect","White"}},
	}
}