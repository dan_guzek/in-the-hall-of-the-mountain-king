return {
	grid = { h=4, w=5 },

	start_point = { 0,5 },

	end_point = { 4,5 },

	pieces = {
		{{"rect","Black"},{"rect","White"},{"rect","White"},{"rect","White"}, {"rect","White"}},
		{{},{},{"rect","White"},{},{"rect","Black"}},
		{{"rect","White"},{"rect","Black"},{"rect","White"},{},{"rect","Black"}},
		{{"rect","Black"},{"rect","Black"},{"rect","Black"},{"rect","White"},"rect","Black"},
	}
}