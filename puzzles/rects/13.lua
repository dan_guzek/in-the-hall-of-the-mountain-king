return {
	grid = { h=5, w=5 },

	start_point = { 0,5 },

	end_point = { 0,0 },

	pieces = {
		{{"rect", "Black"},{"rect", "Black"},{},{},{"rect", "White"}},
		{{"rect", "Black"},{"rect", "Black"},{},{"rect","White"},{}},
		{{},{},{"rect","White"},{"rect","Black"},{"rect", "Black"}},
		{{},{"rect", "White"}, {},{},{}},
		{{"rect", "White"},{},{"rect", "Black"},{"rect", "White"},{}},
	}
}