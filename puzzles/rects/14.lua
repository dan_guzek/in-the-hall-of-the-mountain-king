return {
	grid = { h=5, w=4 },

	start_point = { 0,2 },

	end_point = { 5,0 },

	pieces = {
		{{"rect", "Black"},{"rect", "White"},{"rect","White"},{"rect","Black"}},
		{{"rect", "Black"},{"rect", "White"},{"rect","White"},{"rect","Black"}},
		{{},{},{},{}},
		{{"rect", "White"},{"rect", "Black"},{"rect","Black"},{"rect","White"}},
		{{"rect", "White"},{"rect", "Black"},{"rect","Black"},{"rect","White"}},
	}
}