return {
	grid = { h=4, w=4 },

	start_point = { 3,0 },

	end_point = { 0,0 },

	pieces = {
		{{"rect", "White"},{},{"rect","White"},{"rect","Black"}},
		{{"rect", "Black"},{"rect", "Black"},{"rect","White"},{"rect","Black"}},
		{{},{},{"rect","White"},{"rect","White"}},
		{{"rect", "Black"},{"rect", "Black"},{"rect","Black"},{"rect","Black"}},
	}
}