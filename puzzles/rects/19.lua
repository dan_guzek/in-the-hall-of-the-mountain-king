return {
	grid = { h=4, w=4 },

	start_point = { 4,0 },

	end_point = { 0,4 },

	pieces = {
		{{"rect", "Black"},{"rect","White"},{"rect","Black"},{}},
		{{},{"rect", "White"},{},{}},
		{{},{"rect","Black"},{},{"rect","Black"}},
		{{},{"rect","Black"},{},{"rect","White"}},
	}
}