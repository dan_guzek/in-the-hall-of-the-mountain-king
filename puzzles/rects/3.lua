return {
	grid = { h=4, w=4 },

	start_point = { 2,4 },

	end_point = { 4,0 },

	pieces = {
		{{},{"rect", "Black"},{"rect", "Black"},{} },
		{{"rect","Black"},{"rect", "White"},{"rect", "Black"}, {"rect","White"} },
		{{"rect","White"},{"rect", "White"},{"rect", "Black"},{}},
		{{"rect", "Black"},{"rect", "White"},{"rect", "Black"}, {"rect","White"}},
	}
}