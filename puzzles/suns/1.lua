return {
	grid = { h=4, w=4 },

	start_point = { 4,0 },

	end_point = { 0,4 },

	pieces = {
		{{"sun", "Yellow"},{"sun", "Yellow"},{"sun", "Yellow"},{"sun", "Yellow"}},
		{{"sun", "Yellow"},{"sun", "Yellow"},{"sun", "Black"}, {"sun","Red"}},
		{{"sun", "Black"}, {"sun", "Black"}, {"sun", "Black"}, {"sun","Red"}},
		{{"sun", "Red"},   {"sun", "Red"},	 {"sun", "Red"},   {"sun","Red"}},
	}
}