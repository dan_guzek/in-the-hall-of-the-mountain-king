return {
	grid = { h=4, w=4 },

	start_point = { 4,2 },

	end_point = { 0,2 },

	pieces = {
		{{},{"sun","Orange"},{"sun","Black"},{}},
		{{"sun","Orange"},{"sun","White"},{"sun","White"},{"sun","Black"}},
		{{"sun","Orange"},{"sun","White"},{"sun","White"},{"sun","Black"}},
		{{},{"sun","Orange"},{"sun","Black"},{}},
	}
}