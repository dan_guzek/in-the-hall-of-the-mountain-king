return {
	grid = { h=4, w=4 },

	start_point = { 4,2 },

	end_point = { 0,2 },

	pieces = {
		{{},{"sun","Red"},{"sun","Red"},{"sun","Black"}},
		{{"sun","Red"},{"sun","Black"},{},{"sun","Red"}},
		{{"sun","Red"},{},{"sun","Black"},{"sun","Red"}},
		{{"sun","Black"},{"sun","Red"},{"sun","Red"},{}},
	}
}