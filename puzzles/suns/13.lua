return {
	grid = { h=3, w=5 },

	start_point = { 3,5 },

	end_point = { 0,5 },

	pieces = {
		{{"sun", "Orange"},{"sun", "Black"},{"sun", "Black"},{"sun","Orange"},{}},
		{{"sun", "Orange"},{"sun", "Black"},{"sun", "Black"},{},{}},
		{{"sun", "Orange"},{"sun", "Black"},{"sun", "Black"},{"sun","Orange"},{"sun", "Orange"}},
	}
}