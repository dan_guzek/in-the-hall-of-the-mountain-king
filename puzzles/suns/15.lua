return {
	grid = { h=5, w=3 },

	start_point = { 0,3 },

	end_point = { 5,3 },

	pieces = {
		{{"sun", "Black"},{},{}},
		{{"sun", "Black"},{"sun", "Orange"},{"sun", "Orange"}},
		{{},{"sun", "Orange"},{"sun", "Orange"}},
		{{"sun", "Orange"},{"sun", "Orange"},{"sun", "Black"}},
		{{"sun", "Black"},{"sun", "Black"},{"sun", "Black"}},
	}
}