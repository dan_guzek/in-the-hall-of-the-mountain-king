return {
	grid = { h=4, w=4 },

	start_point = { 4,0 },

	end_point = { 0,4 },

	pieces = {
		{{"sun", "Black"},{"sun", "Black"},{"sun", "Black"},{"sun", "Black"}},
		{{},{"sun", "White"},{"sun", "White"},{}},
		{{},{"sun", "White"},{"sun", "White"},{}},
		{{"sun", "Black"},{"sun", "Black"},{"sun", "Black"}, {"sun","Black"}},
	}
}