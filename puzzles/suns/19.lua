return {
	grid = { h=4, w=4 },

	start_point = { 4,0 },

	end_point = { 0,4 },

	pieces = {
		{{"sun", "Orange"},{"sun", "White"},{"sun", "Orange"},{"sun", "White"}},
		{{"sun", "Black"},{"sun", "Orange"},{"sun", "White"},{"sun","Black"}},
		{{"sun", "Orange"},{"sun", "Black"},{"sun", "Black"},{"sun","Orange"}},
		{{"sun", "Black"},{"sun", "White"},{"sun", "Orange"}, {"sun","Black"}},
	}
}