return {
	grid = { h=3, w=3 },

	start_point = { 3,0 },

	end_point = { 0,3 },

	pieces = {
		{{"sun", "Blue"},{"sun","Blue"},{"sun", "Yellow"}},
		{{"sun", "Yellow"},{"sun", "Blue"},{"sun", "Yellow"}},
		{{"sun", "Blue"}, {"sun", "Yellow"}, {}},
	}
}