return {
	grid = { h=5, w=5 },

	start_point = { 5,0 },

	end_point = { 0,5 },

	pieces = {
		{{"sun","White"},{"sun","Red"},{},{"sun","Red"},{"sun","White"}},
		{{"sun","Red"},{},{"sun","White"},{},{"sun","Red"}},
		{{},{"sun","White"},{},{"sun","White"},{}},
		{{"sun","Red"},{},{"sun","White"},{},{"sun","Red"}},
		{{"sun","White"},{"sun","Red"},{},{"sun","Red"},{"sun","White"}},
	}
}