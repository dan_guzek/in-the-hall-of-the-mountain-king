return {
	grid = { h=5, w=5 },

	start_point = { 5,0 },

	end_point = { 0,5 },

	pieces = {
		{{},{"sun","Yellow"},{},{"sun","Yellow"},{}},
		{{"sun","Black"},{"sun","Blue"},{"sun","Red"},{"sun","Blue"},{"sun","White"}},
		{{"sun","Black"},{"sun","Blue"},{"sun","Red"},{"sun","Blue"},{"sun","White"}},
		{{"sun","Black"},{"sun","White"},{"sun","White"},{"sun","Black"},{"sun","Black"}},
		{{"sun","Black"},{"sun","White"},{"sun","White"},{"sun","Black"},{"sun","Black"}},
	}
}