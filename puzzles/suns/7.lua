return {
	grid = { h=4, w=4 },

	start_point = { 4,2 },

	end_point = { 0,2 },

	pieces = {
		{{},{},{"sun","Black"},{"sun","Orange"}},
		{{},{},{},{"sun","Black"}},
		{{"sun","Black"},{},{},{}},
		{{"sun","Orange"},{"sun","Black"},{},{}},
	}
}