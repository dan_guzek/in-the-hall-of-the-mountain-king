return {
	grid = { h=4, w=4 },

	start_point = { 2,0 },

	end_point = { 2,4 },

	pieces = {
		{{},{"sun","Red"},{"sun","Red"},{}},
		{{"sun","Red"},{"sun","Black"},{"sun","Black"},{"sun","Red"}},
		{{"sun","White"},{"sun","Black"},{"sun","Black"},{"sun","White"}},
		{{},{"sun","White"},{"sun","White"},{}},
	}
}