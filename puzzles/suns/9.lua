return {
	grid = { h=3, w=4 },

	start_point = { 3,2 },

	end_point = { 0,2 },

	pieces = {
		{{"sun","White"},{"sun","White"},{"sun","Black"},{"sun","Black"}},
		{{"sun","White"},{"sun","White"},{"sun","Orange"},{"sun","Orange"}},
		{{"sun","Orange"},{"sun","Orange"},{"sun","Orange"},{"sun","Orange"}},
	}
}